#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2021 Michel Le Bihan <michel@lebihan.pl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This script validates the JSON files and applies a consistent format to them.

It is intended as a Git pre-commit hook.
"""

import sys

from common import *

PROPERTIES = [
	"lastCheck",
	"website",
	"busFactor",
	"company",
	"passwordReset",
	"inBandRegistration",
	"registrationWebPage",
	"ratingXmppComplianceTester",
	"ratingImObservatoryClientToServer",
	"ratingImObservatoryServerToServer",
	"maximumHttpFileUploadFileSize",
	"maximumHttpFileUploadTotalSize",
	"maximumHttpFileUploadStorageTime",
	"maximumMessageArchiveManagementStorageTime",
	"professionalHosting",
	"freeOfCharge",
	"legalNotice",
	"serverLocations",
	"groupChatSupport",
	"chatSupport",
	"emailSupport",
	"since",
]

# Set "logging.DEBUG" for debug output.
LOG_LEVEL = logging.INFO

logging.basicConfig(level=LOG_LEVEL, format="%(levelname)-8s %(message)s")

for json_file_path in PROVIDERS_FILE_PATH, CLIENTS_FILE_PATH:
	with open(json_file_path, "r+") as json_file:
		try:
			original_json_string = json_file.read()

			# The entries (providers or clients) are sorted in alphabetically ascending and lowercase first order by their keys (domains or names).
			# The properties are sorted in the order specified by the variable PROPERTIES.
			# A newline is appended because Python's JSON module does not add one.
			unsorted_entries = json.loads(original_json_string).items()
			sorted_entries = {entry_key: {property_key: properties[property_key] for property_key in PROPERTIES if property_key in properties} for entry_key, properties in sorted(unsorted_entries, key=lambda item:str.swapcase(item[0]))}
			formatted_json_string = json.dumps(sorted_entries, indent=JSON_OUTPUT_INDENTATION) + "\n"

			properties_missing = False

			# Check if provider entries have all needed properties.
			if json_file_path == PROVIDERS_FILE_PATH:
				for provider, properties in sorted_entries.items():
					for property_key in PROPERTIES:
						if property_key not in properties.keys():
							logging.error("%s: Property '%s' is missing" % (provider, property_key))
							if not properties_missing:
								properties_missing = True

			if original_json_string == formatted_json_string and not properties_missing:
				logging.debug("'%s' is already correctly formatted" % json_file_path)
			else:
				json_file.seek(0)
				json_file.write(formatted_json_string)
				json_file.truncate()

				if properties_missing:
					logging.error("'%s' has missing provider properties: Add them before the next commit" % json_file_path)

				if original_json_string != formatted_json_string:
					logging.info("'%s' has been formatted: Stage changed lines by 'git add -p %s' and run 'git commit'" % (json_file_path, json_file_path))

				sys.exit(1)
		except json.decoder.JSONDecodeError as e:
			logging.error("'%s' has invalid JSON syntax: %s in line %s at column %s: Correct syntax, stage changed lines by 'git add -p %s' and run 'git commit' again" % (json_file_path, e.msg, e.lineno, e.colno, json_file_path))
			sys.exit(1)
